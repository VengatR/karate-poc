function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);

  if (!env) {
    env = 'dev';
  }

  var envData =  read('classpath:config/' + env + '/' + env + '-config.json')
  //var envData = read('src/config/'+ env + '/' + env + '-config.json')

  var android = {}
  android["desiredConfig"] = {
    "app" : "https://github.com/babusekaran/droidAction/raw/main/build/UiDemo.apk",
    "newCommandTimeout" : 300,
    "platformVersion" : "10.0",
    "platformName" : "Android",
    "connectHardwareKeyboard" : true,
    "deviceName" : "emulator-5554",
    "noReset": "true",
    //"appPackage": "io.jungleerummy.jungleegames",
    //"appActivity": ".MainActivity",
    "udid": "94d71d62",
    "automationName" : "UiAutomator2"
   }
   
  var config = {
    env: env,
    jungleeBaseUrl: "https://www.jungleerummy.com",
    
    serviceBaseUrl: '',
    transactionId: ''
  }

  config.serviceBaseUrl = envData.baseUrl
  config["android"] = android

  //TODO: Delete this block of code once the env data is gathered in json and all data are accounted for
  // if (env == 'dev') {
  //   // customize
  //   // e.g. config.foo = 'bar';
  //   config.serviceBaseUrl = envData.baseUrl
  // } else if (env == 'e2e') {
  //   config.serviceBaseUrl = envData.baseUrl
  //   // customize
  // } else {
  //   config.serviceBaseUrl = envData.baseUrl
  // }

  var uuid = envData.app_id + '-AUTOMATION-' + java.util.UUID.randomUUID();
  config.transactionId = uuid;
  /**
   * To get auth token, it will be evaluated only once and cached for subsequent requests
   * We create default authorization tokens with token-generator.js, which internally uses authorization.feature file to obtain tokens
   */
  //config.default_tokens = karate.callSingle('classpath:token-generator.js', config);

  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}