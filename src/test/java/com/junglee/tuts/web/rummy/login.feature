Feature: Rummy login with username and password

    This is a common feature that logs into junglee rummy application

    Background: Launch browser with junglee rummy app
    # steps here are executed before each Scenario in this file
    # variables defined here will be 'global' to all scenarios
    # and will be re-initialized before every scenario
    # We might well form the base url or landing page here

    # * configure driverTarget = { docker: 'justinribeiro/chrome-headless', showDriverLog: true }
    # * configure driverTarget = { docker: 'ptrthomas/karate-chrome', showDriverLog: true }
    # * configure driver = { type: 'chromedriver', showDriverLog: true }
    # * configure driver = { type: 'geckodriver', showDriverLog: true }
    # * configure driver = { type: 'safaridriver', showDriverLog: true }
    # * configure driver = { type: 'iedriver', showDriverLog: true, httpConfig: { readTimeout: 120000 } }

    * configure driver = { type: 'chrome', showDriverLog: true }
    Given def url = https://www.jungleerummy.com/

    Scenario: Login with username and password
        Given driver url
        And driver.maximize()
        And input('input[id=username]', 'vengatramanan@gmail.com')
        And input('input[id=password]', 'test123')
        When submit().click('//div[class=\"login-btn-header\"]/button')
        Then retry(5, 10000).waitForUrl('https://www.jungleerummy.com/promotions')
        # When click('//div[class=\"popup-close-btn\"]')
        And var popUpExists = exists('div.popup-content-wrapper.ng-star-inserted > div')
        When if(popUpExists) click('//div[class=\"popup-close-btn\"]')



