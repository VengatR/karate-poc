package com.junglee.tuts.web.rummy.promotions;

import com.intuit.karate.junit5.Karate;

public class PromotionsTest {

    @Karate.Test
    Karate testPromotions() {
        return Karate.run("promotions").relativeTo(getClass());
    }

}
