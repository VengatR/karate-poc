Feature: Jummy promotion features will be tested in this file


  Background:

    # this section is optional !
    # steps here are executed before each Scenario in this file
    # variables defined here will be 'global' to all scenarios
    # and will be re-initialized before every scenario
    # We might well form the base url or landing page here

    # * configure driverTarget = { docker: 'justinribeiro/chrome-headless', showDriverLog: true }
    # * configure driverTarget = { docker: 'ptrthomas/karate-chrome', showDriverLog: true }
    # * configure driver = { type: 'chromedriver', showDriverLog: true }
    # * configure driver = { type: 'geckodriver', showDriverLog: true }
    # * configure driver = { type: 'safaridriver', showDriverLog: true }
    # * configure driver = { type: 'iedriver', showDriverLog: true, httpConfig: { readTimeout: 120000 } }

    #* configure driver = { type: 'chrome', showDriverLog: true }
    #  Given def url = https://www.jungleerummy.com/
    * call read('login.feature')

    Scenario: Login and navigate to promotions tab
      And waitForText('div#test_promo > h1', 'Rummy Promotions')
      And match text('div#test_promo > h1') == 'Rummy Promotions'
      Then call read('logout.feature')
