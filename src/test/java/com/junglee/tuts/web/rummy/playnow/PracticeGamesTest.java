package com.junglee.tuts.web.rummy.playnow;

import com.intuit.karate.junit5.Karate;

public class PracticeGamesTest {

    @Karate.Test
    Karate testPracticeGames() {
        return Karate.run("practicegames").relativeTo(getClass());
    }
}
