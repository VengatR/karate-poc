Feature: features under the practice games will be tested
  These are selenium tests
  These are sample tests


  Background:
    # this section is optional !
    # steps here are executed before each Scenario in this file
    # variables defined here will be 'global' to all scenarios
    # and will be re-initialized before every scenario
    # We might well form the base url or landing page here

    # * configure driverTarget = { docker: 'justinribeiro/chrome-headless', showDriverLog: true }
    # * configure driverTarget = { docker: 'ptrthomas/karate-chrome', showDriverLog: true }
    # * configure driver = { type: 'chromedriver', showDriverLog: true }
    # * configure driver = { type: 'geckodriver', showDriverLog: true }
    # * configure driver = { type: 'safaridriver', showDriverLog: true }
    # * configure driver = { type: 'iedriver', showDriverLog: true, httpConfig: { readTimeout: 120000 } }

    * configure driver = { type: 'chrome', showDriverLog: true }
    Given def url = https://www.jungleerummy.com/

  Scenario Outline: Launch the rummy page, login with username and pwd and click practice games. Verify the feature is available
    Given driver url
    And driver.maximize()
    And input('input[id=username]', <username>)
    And input('input[id=password]', <password>)
    When submit().click('//div[class=\"login-btn-header\"]/button')
    Then retry(5, 10000).waitForUrl('https://www.jungleerummy.com/promotions')
    When click('//div[class=\"popup-close-btn\"]')
    And def practiceGamesLink = waitFor('//a[href=\"client/lobby/practice-games\"]/div[class="\tab-name\"]')
    And practiceGamesLink.click()
    Then match html(tr > th.col-name.sortable.sort-hide-left) == '<th class="col-name sortable sort-hide-left"> Name </th>'
    Examples:
      | username                | password | description  |
      | vengatramanan@gmail.com | test123  | valid user   |
      | invalid                 | invalid  | invalid user |



  Scenario: Launch the rummy page, login and click Tournaments
    Given driver url
    And driver.maximize()
    And input('input[id=username]', 'vengatramanan@gmail.com')
    And input('input[id=password]', 'test123')
    When submit().input('input[id=password]', Key.ENTER)
    Then waitForUrl('https://www.jungleerummy.com/promotions')
    When click('//div[class=\"popup-close-btn\"]')
    And def tournamentsLink = waitFor('//a[href=\"client/lobby/tournaments\"]/div[class=\"tab-name\"]')
    And click('//a[href=\"client/lobby/tournaments\"]/div[class=\"tab-name\"]')
    Then match text('tr:nth-child(2) > td.col-name') == 'Points Rummy'


