package com.junglee.tuts.perf.withdrawal

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import scala.concurrent.duration._

class WithdrawalSimulation extends Simulation {

  val withdrawal = scenario(scenarioName = "Send a create withdrawal request with valid request payload").exec(karateFeature("classpath:com/junglee/tuts/perf/witdrawal/withdrawal.feature"))

  setUp(
      withdrawal.inject(rampUsers(10) during (5 seconds))
  )

//   val protocol = karateProtocol(
//     "/cats/{id}" -> Nil,
//     "/cats" -> pauseFor("get" -> 15, "post" -> 25)
//   )

//   protocol.nameResolver = (req, ctx) => req.getHeader("karate-name")
//   protocol.runner.karateEnv("perf")

//   val create = scenario("create").exec(karateFeature("classpath:com/junglee/tuts/perf/witdrawal/withdrawal.feature"))
//   val delete = scenario("delete").exec(karateFeature("classpath:mock/cats-delete.feature@name=delete"))

//   setUp(
//     create.inject(rampUsers(10) during (5 seconds)).protocols(protocol),
//     delete.inject(rampUsers(5) during (5 seconds)).protocols(protocol)
//   )

}
