Feature: This suite is to test withdrawal servcie (standalone) 

  Background:
    * def getRandomUUID = function(){ return java.util.UUID.randomUUID() + '' }
    Given def withdrawalRequest = read('withdrawalPayload.json')
    And url 'http://withdrawal.qa-1.jungleerummyqa.com/'
    And header Content-Type = 'application/json'
    And header X-REQUEST-ID = getRandomUUID()
    And header X-PRODUCT-ID = 'RUMMY'
    And header X-USER-ID = '1964846'
  	
Scenario: Send a create withdrawal request with valid request payload
  Given path 'withdrawals/v1'
  And request withdrawalRequest
  And print 'Withdrawal request for valid payload is : ', withdrawalRequest
  When method POST
  Then status 200

Scenario: Send a create withdrawal request with account_holder_name as blank/empty in request payload
  Given path 'withdrawals/v1'
  And request withdrawalRequest
  And set withdrawalRequest.account_holder_name = ''
  And print 'Withdrawal request for payload with account_holder_name as null is : ', withdrawalRequest
  When method POST
  Then status 400