package com.junglee.tuts.rest.withdrawal;

import com.intuit.karate.junit5.Karate;

public class WithdrawalTestRunner {
	   @Karate.Test
	    Karate testAuthService() {
	        return Karate.run("withdrawal").relativeTo(getClass());
	    }
}
