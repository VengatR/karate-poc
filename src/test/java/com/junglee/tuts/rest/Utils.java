package com.junglee.tuts.rest;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    public static String encodeUrl(String url) {
        String encodedUrl = null;
        try {
            encodedUrl = URLEncoder.encode(url, StandardCharsets.UTF_8.toString()); 
        } catch(UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return encodedUrl;
    }

    public static String getAuthCode(String urlWithCode) throws Exception {
        Pattern pattern = Pattern.compile("code=.*&");
		Matcher matcher = pattern.matcher(urlWithCode);
		String authorisationCode = matcher.group(1);
		authorisationCode = authorisationCode.replaceFirst("code=", "").replaceAll("&$", "");
        return authorisationCode;
    }
    
}
