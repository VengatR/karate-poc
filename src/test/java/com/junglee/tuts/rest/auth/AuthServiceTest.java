package com.junglee.tuts.rest.auth;

import com.intuit.karate.junit5.Karate;

public class AuthServiceTest {

    @Karate.Test
    Karate testAuthService() {
        return Karate.run("authservice").relativeTo(getClass());
    }
}
