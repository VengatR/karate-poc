Feature: This test suite will test the oauth orchestration with FB/Google as Identitiy provider
  Also with Junglees own access tokens. These are api tests

  Background:
    # this section is optional !
    # steps here are executed before each Scenario in this file
    # variables defined here will be 'global' to all scenarios
    # and will be re-initialized before every scenario
    # We might well form the base url or landing page here

    * def baseUrl = 'http://172.25.6.140:3000/'
    * def clientId = 'some_client_id'
    * def client_secret = 'some_client_secret'
    * def redirectUri = 'some_redirect_uri'
    * def scope = 'some_scope'
    * def username = 'some_email'
    * def password = 'some_password'
    * def baseUrl = 'http://172.25.6.140:3000/'
    * def googleLoginUri = 'https://accounts.google.com/o/oauth2/v2/auth/identifier?response_type=code&client_id='+ clientId + '&scope=openid email https://www.googleapis.com/auth/userinfo.profile&redirect_uri='+ redirectUri + '&state=b938618bf66d772698b758157067fe7d&flowName=GeneralOAuthFlow'
    * def encodeUrl = 
      """
        function(url) {
          var Utils = Java.type('com.junglee.tuts.rest.Utils')
          return Utils.encodeUrl(url)
        }
      """
    * def getAuthCode = 
    """
        function(urlWithAuthCode) {
          var Utils = Java.type('com.junglee.tuts.rest.Utils')
          return Utils.getAuthCode(urlWithAuthCode)
        }
    """
    * def googleEncodedUrl = call   encodeUrl googleLoginUri
    * def sleep =
      """
      function(seconds){
        for(i = 0; i <= seconds; i++)
        {
          java.lang.Thread.sleep(1*1000);
          karate.log(i);
        }
      }
      """
    * configure driver = { type: 'chrome', showDriverLog: true }

  Scenario Outline: Get auth code
    Given url baseUrl + '/oauth2/authorize'
    And def myAuthFunction = 
    """
      function(creds) {
        var temp = creds.username + ':' + creds.password;
        var Base64 = Java.type('java.util.Base64');
        var encoded = Base64.getEncoder().encodeToString(temp.getBytes());
        return 'Basic ' + encoded;
      }
    """
    #And header Authorization = call read('authservice.js') { username: 'john', password: 'secret' }
    And header Authorization = myAuthFunction { username: <usernamee>, password: <password> }
    And header Content-Type = 'application/x-www-form-urlencoded'
    And form field response_type = 'code'
    And request {client_id': <client_id>, 'redirect_uri' : <redirect_uri>, 'scope' : <scope>}
    And header Accept = 'application/json'
    When method POST
    * print response
    Then response.status == <status_code>
    And def authToken = response.token
    Examples:
      | username | password | status_code | description  |
      | test123  | test123  | 200         | valid user   |
      | invalid  | invalid  | 400         | invalid user |

    Scenario Outline: Get auth code and access token
      Given url baseUrl + '/oauth2/authorize'
      And header Authorization = call read('authservice.js') { username: <username>, password: <password> }
      And header Content-Type = 'application/x-www-form-urlencoded'
      And form field response_type = 'code'
      And request {'client_id': <client_id>, 'redirect_uri' : <redirect_uri>, 'scope' : <scope>}
      And header Accept = 'application/json'
      When method POST
        * print response
      Then response.status == <status_code>
      And def authToken = response.code

      Given url baseUrl + '/oauth2/token'
      And header Authorization = call read('authservice.js') { username: <username>, password: <password> }
      And header Content-Type = 'application/x-www-form-urlencoded'
      And request {'code' : authToken, 'redirect_uri' : <redirect_uri>, 'grant_type' : authorization_code}
      When method POST
      Then status 200
      And defAccesstoken = response.access_token
      Examples:
          | username | password | status_code | description |
          | test123 |  test123  | 200         | valid user  |
          | invalid |  invalid  | 400         | invalid user |



    Scenario: Get access token with google flow
        Given driver googleEncodedUrl
        And input('input[type=email]', 'vengatramanan@gmail.com')
        When click('button[type=button]')
        And input('input[type=password]', 'test123')
        Then input('input[type=password]', Key.ENTER)
        * sleep(3)
        * def currentUrl = driver.url
        * def code = getAuthCode currentUrl
        Given url 'https://www.googleapis.com/oauth2/v4/token'
        And request{'code' : code, 'cliemt_id' : clientId, 'client_secret' : client_secret, 'redirect_uri' : redirectUri, 'grant_type' : 'authorization_code'}
        When method POST
        * print response
        Then status 200
        And def accessToken = response.id_token


        


