package com.junglee.tuts.mobile;

import com.intuit.karate.junit5.Karate;

public class SampleTestRunner {
    @Karate.Test
    Karate testAuthService() {
        return Karate.run("sampletest").relativeTo(getClass());
    }
}
